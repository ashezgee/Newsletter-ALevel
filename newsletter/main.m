//
//  main.m
//  newsletter
//
//  Created by h3adsh0tzz on 18/09/2017.
//  Copyright © 2017 h3adsh0tzz. All rights reserved.
//
#import<foundation/foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        
    }
}
